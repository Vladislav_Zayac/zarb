﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireController : MonoBehaviour
{
    [SerializeField] private Player _player = null;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _player.Attack();
        }
    }
}
