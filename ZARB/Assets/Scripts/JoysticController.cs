﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoysticController : MonoBehaviour
{
    [SerializeField] private GameObject _touchMarker = null;
    [SerializeField] private Player _player = null;
    private Vector3 _targetVector3;

    private void Start()
    {
        _touchMarker.transform.position = transform.position;
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 touchPos = Input.mousePosition;
            _targetVector3 = touchPos - transform.position;

            if (_targetVector3.magnitude < 150)
            {
                _touchMarker.transform.position = touchPos;
                _player.targetMove = _targetVector3;
            }
        }
        else
        {
            _touchMarker.transform.position = transform.position;
            _player.targetMove = new Vector3(0,0,0);
        }
    }
}
