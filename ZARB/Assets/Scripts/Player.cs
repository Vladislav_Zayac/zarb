﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private float _speed = 0f;
    //[SerializeField] private InputHandler _inputHandler = null;
    [HideInInspector]public Vector3 targetMove;

    private void Update()
    {
        Vector3 targetVector = new Vector3(targetMove.x,0,targetMove.y);
        transform.Translate(_speed * Time.deltaTime * targetVector);
    }

    public void Attack()
    {
        Debug.Log("Attack!");
    }
}
